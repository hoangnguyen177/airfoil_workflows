#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>



double fH(double);
double fL(double);
double fU(double, double, double, double);
double fPGP(double);
double fDELTA(double);
double fTE(double, double, double, double, double);
double fTRONSET(double, double, double);
double fprofileU(int, double, double);
double fprofileL(int, double, double);

void fturbSkinU(int mb, double* s, double* rex, double* x_cU, double* sti, double* interm_fact, double* cf_total, double* lamda_trans, int* itrans)
{
	FILE *fptU;
	fptU = fopen("turb_skin_fric_upper_surface.dat", "w");
	fprintf(fptU, "\nIncompressible laminar flow over the airfoil\n\n");
	fprintf(fptU, "\ni  s[i]     Rex          x/c	 interm_fact      Cf (turbulent skin friction coefficient)n\n");
	int i;
	for(i=1; i < mb; i++){
		if(i==itrans[0]){
			fprintf(fptU, "\nEstimated Length of the Transition Zone (measured along the panels): lamda_trans = %lf\n", lamda_trans[0]);
			fprintf(fptU, "Distance of Transition point from the stagnation point (measured along the panels)= %lf\n\n", sti[0]);
			}
		fprintf(fptU, "%d %lf %lf %lf %lf %lf\n", i, s[i], rex[i], x_cU[i], interm_fact[i], cf_total[i]);
		}
	fclose(fptU);
	
	FILE *fsL, *fTr, *fTu;
	fsL = fopen("skin_fric_upper_surface_laminar.dat", "w");
	fTr = fopen("skin_fric_upper_surface_transition.dat", "w");
	fTu = fopen("skin_fric_upper_surface_turbulence.dat", "w");
	
	int first =1;
	for(i=1; i < mb; i++){
		if(interm_fact[i]==0) fprintf(fsL, "%lf %lf\n", x_cU[i], cf_total[i]);
		if(i==itrans[0]) fprintf(fTr, "%lf %lf\n", x_cU[i], cf_total[i]);  // for avoiding any gaps in the graphs
		if(interm_fact[i]>0 && interm_fact[i]<1) fprintf(fTr, "%lf %lf\n", x_cU[i], cf_total[i]);
		if(interm_fact[i]==1){
			fprintf(fTu, "%lf %lf\n", x_cU[i], cf_total[i]);
			if(first==1) fprintf(fTr, "%lf %lf\n", x_cU[i], cf_total[i]); // for avoiding any gaps in the graphs
			first=2;
		}
	}
	fclose(fsL);
	fclose(fTr);
	fclose(fTu);
	
	
}


void fturbSkinL(int mb, double* s, double* rex, double* x_cL, double* sti, double* interm_fact, double* cf_total, double* lamda_trans, int* itrans)
{
	FILE *fptL;
	fptL = fopen("turb_skin_fric_lower_surface.dat", "w");
	fprintf(fptL, "\nIncompressible laminar flow over the airfoil\n\n");
	fprintf(fptL, "\ni  s[i]     Rex          x/c	 interm_fact      Cf (turbulent skin friction coefficient)\n");
	int i;
	for(i=1; i < mb; i++){
		if(i==itrans[1]){
			fprintf(fptL, "\nEstimated Length of the Transition Zone (measured along the panels): lamda_trans = %lf\n", lamda_trans[1]);
			fprintf(fptL, "Distance of Transition point from the stagnation point (measured along the panels)= %lf\n\n", sti[1]);
			}
		fprintf(fptL, "%d %lf %lf %lf %lf %lf\n", i, s[i], rex[i], x_cL[i], interm_fact[i], cf_total[i]);
		}
	fclose(fptL);
	
	FILE *fsL, *fTr, *fTu;
	fsL = fopen("skin_fric_lower_surface_laminar.dat", "w");
	fTr = fopen("skin_fric_lower_surface_transition.dat", "w");
	fTu = fopen("skin_fric_lower_surface_turbulence.dat", "w");
	
	int first =1;
	for(i=1; i < mb; i++){
		if(interm_fact[i]==0) fprintf(fsL, "%lf %lf\n", x_cL[i], cf_total[i]);
		if(i==itrans[1]) fprintf(fTr, "%lf %lf\n", x_cL[i], cf_total[i]);  // for avoiding any gaps in the graphs
		if(interm_fact[i]>0 && interm_fact[i]<1) fprintf(fTr, "%lf %lf\n", x_cL[i], cf_total[i]);
		if(interm_fact[i]==1){
			fprintf(fTu, "%lf %lf\n", x_cL[i], cf_total[i]);
			if(first==1) fprintf(fTr, "%lf %lf\n", x_cL[i], cf_total[i]); // for avoiding any gaps in the graphs
			first=2;
		}
	}
	fclose(fsL);
	fclose(fTr);
	fclose(fTu);
	
}


void fparameterL(int mb, double* delta, double* theta, double* H, double* cf, double* x_cL, double* ret, double* lambda)
{
	FILE *fpbL;
	fpbL = fopen("bl_parameters_lower_surface.dat", "w");
	fprintf(fpbL, "\nIncompressible laminar flow over the airfoil\n\n");
	fprintf(fpbL, "\ni   delta_c     theta_c     H_c     cf_c    x/c    Retheta      lambda\n\n");
	int i;
	for(i=1; i < mb; i++){
		fprintf(fpbL, "%d %lf %lf %lf %lf %lf %lf %lf\n", i, delta[i], theta[i], H[i], cf[i], x_cL[i], ret[i], lambda[i]);
		}
	fclose(fpbL);
}


void fparameterU(int mb, double* delta, double* theta, double* H, double* cf, double* x_cU, double* ret, double* lambda)
{
	FILE *fpbU;
	fpbU = fopen("bl_parameters_upper_surface.dat", "w");
	fprintf(fpbU, "\nIncompressible laminar flow over the airfoil\n\n");
	fprintf(fpbU, "\ni   delta_c     theta_c     H_c     cf_c    x/c    Retheta      lambda\n\n");
	int i;
	for(i=1; i < mb; i++){
		fprintf(fpbU, "%d %lf %lf %lf %lf %lf %lf %lf\n", i, delta[i], theta[i], H[i], cf[i], x_cU[i], ret[i], lambda[i]);
		}
	fclose(fpbU);
}


void fZones(double* x_cU, double* x_cL, double* y_cU, double* y_cL, int np_us, int np_ls, int* itrans, double* lamda_trans, double* sti){

	FILE *fzlL, *fzlU;
	fzlL = fopen("zoneLaminarLower.dat", "w");
	fzlU = fopen("zoneLaminarUpper.dat", "w");
	int i,j;
	for(i=itrans[1]; i>-1; i--) fprintf(fzlL, "%lf\t%lf \n", x_cL[i], y_cL[i]);
	for(j=0; j<itrans[0]+1; j++) fprintf(fzlU, "%lf\t%lf \n", x_cU[j], y_cU[j]);
	fclose(fzlU);
	fclose(fzlL);
	
		int pU=0; // For predicting the end of transition zone at Upper Surface
		for(pU=itrans[0]; pU<np_us; pU++){
				if(x_cU[pU]>=(lamda_trans[0] + sti[0])) break;
		}
		FILE *fzTrU;
		fzTrU = fopen("zoneTurbulenceUpper.dat", "w");
		for(i=pU; i<np_us; i++) fprintf(fzTrU, "%lf\t%lf \n", x_cU[i], y_cU[i]);
		fclose(fzTrU);
	
		int pL=0; // For predicting the end of transition zone at Lower Surface
		for(pL=itrans[1]; pL<np_ls; pL++){
				if(x_cL[pL]>=(lamda_trans[1] + sti[1])) break;
		}
		FILE *fzTrL;
		fzTrL = fopen("zoneTurbulenceLower.dat", "w");
		for(i=pL; i<np_ls; i++) fprintf(fzTrL, "%lf\t%lf \n", x_cL[i], y_cL[i]);
		fclose(fzTrL);
	
	
	
	FILE *fzTU; FILE *fzTL; //For Plotting the Transition Zones
	fzTU = fopen("zoneTransitionUpper.dat", "w");
	fzTL = fopen("zoneTransitionLower.dat", "w");
	for(i=itrans[1]; i<pL+1; i++) fprintf(fzTL, "%lf\t%lf \n", x_cL[i], y_cL[i]);
	for(j=itrans[0]; j<pU+1; j++) fprintf(fzTU, "%lf\t%lf \n", x_cU[j], y_cU[j]);
	fclose(fzTU);
	fclose(fzTL);
	
		
}

//function to calculate factorial of the given number
double facto (int n)
{
double f = 1;
int i;
	for(i=2 ;i<=n;i++)
	{
	f*=i;
	}
//printf("%lf\n", f);
return f;
}

//function to calculate the functions used in bezier generation
double bfunc (double t, int n, double *x)
{
double f= 0;
int j;
	for (j = 0; j<=n; j++)
	{
		double nj = n-j;
		double t1 = 1-t;
		double jj = j;
		
		f  = f + facto(n)/facto(n-j)/facto(j)*pow(t1,nj)*pow(t,jj)*x[j];
	}
return f;
}

int gloflag =0;


//function to generate the coordinates of the airfoil using bezier curve
void bezier(int nc, int na, double *cx, double *cy, double *ax, double *ay)

{

int n=70;		// no of points the bezier curve is divided into
double t = 0;		//bezier function parameter
double ninv = pow((n-1),-1);
double coord[n];


double x [n];	// airfoil x points
double camy [n]; // camberline y points
double airy [n]; // thickness y points
double yu [n]; // airfoil y points
double yl [n]; // airfoil y points

double ak, bk, ck, fak, fbk, fck;

double maxc=0;
ak =0;
bk = 1;

int i,j,k;

FILE *fp;


fp = fopen( "x_input.dat", "r" ); 		//read 70 points from which to calculate the airfoil shape  

for (i = 0; i < n; i++)
  { 				
	fscanf(fp, "%lf", &x[i]); 
//	x[i] = i*ninv;
  }
fclose(fp);



// bezier curve for the foil thickness

ak =0;
bk = 1;

for (i = 0; i<n; i++)
{
	for (k = 0; k<100; k++)
	{
		fak = bfunc(ak,na-1,ax)-x[i];
		fbk = bfunc(bk,na-1,ax)-x[i];
	
		ck = (fbk*ak - fak*bk) / (fbk - fak);
		fck = bfunc(ck,na-1,ax)-x[i];

		if ( (fak>=0&&fck>=0)||(fak<0&&fck<0))
			{ak = ck;}
		else
			{bk = ck;}
		if ( (bk - ak) < 0.000001 )
			break;

	}
	coord[i] = ck;

}


for (i = 0; i<n; i++)
{
	t = coord[i];
	airy[i] = bfunc(t,(na-1),ay);

//airy[i] = 0;
}


// bezier curve for the airfoil camber

ak =0;
bk = 1;

for (i = 0; i<n; i++)
{
	for (k = 0; k<100; k++)
	{
		fak = bfunc(ak,nc-1,cx)-x[i];
		fbk = bfunc(bk,nc-1,cx)-x[i];
	
		ck = (fbk*ak - fak*bk) / (fbk - fak);
		fck = bfunc(ck,nc-1,cx)-x[i];

		if ( (fak>=0&&fck>=0)||(fak<0&&fck<0))
			{ak = ck;}
		else
			{bk = ck;}
		if ( (bk - ak) < 0.000001 )
			break;

	}
	coord[i] = ck;

}


for (i = 0; i<n; i++)
{
	t = coord[i];
	camy[i] = bfunc(t,(nc-1),cy);
	if( maxc < fabs(camy[i]))
		maxc = fabs(camy[i]);
}





//creating airfoil

for (i =0; i<n; i++)
{
	yu[i] = camy[i] + airy[i];
	yl[i] = camy[i] - airy[i];
}

gloflag =1;
//start checking
// check for minimum thickness
for (i =0; i<n; i++)
{
	if(((yu[i] - yl[i]) >=0.07)&&((yu[i] - yl[i]) >=2*maxc))
	{
		gloflag =0;
		break;
	}
	
}


// check for overlapping lines
for (i =0; i<n; i++)
{
	if( airy[i] < 0)
{
	gloflag = 1;
        printf("Overlapping line not met\n");
	break;
}
if( camy[i]>0.1||camy[i]<-0.1)
{
	gloflag = 1;
        printf("Invalid camry\n");
	break;
}

if(yu[i] <0)
{
	gloflag = 1;
        printf("Invalid yu\n");
	break;
}
}

// check for max thickness
for (i =0; i<n; i++)
{
	if( (yu[i] - yl[i]) >0.5)
	{
		gloflag =1;
                printf("max thickness invalid\n");
		break;
	}	
	
}


// check for tapering trailing edge
for (i =0; i<12; i++)
{
	if( (yu[i] - yl[i]) >0.2)
	{
		gloflag =1;
                printf("tailing edge invalid\n");
		break;
	}	
}
for (i =0; i<5; i++)
{
	if((yu[i]>0.06)||(yl[i]<-0.06))
	{
		gloflag =1;
                printf("invalid yu trailing edge\n");
		break;
	}	
}

// check for smooth leading edge
for (i =n-1; i>n-6; i--)
{
	if( (yu[i] - yl[i]) > 0.11)
	{
		gloflag =1;
                printf("leading edge invalid\n");
		break;
	}	
}
//check for complete airfoils
for (i =5; i<n-4; i++)
{
	if( (yu[i] - yl[i]) <=0.01)
	{
		gloflag =1;
                printf("complete airfoils invalid\n");
		break;
	}	
}






FILE *fpa; 		// File pointer DECLARATION

fpa = fopen("camber_surface.dat", "w"); 	

for(i = 0; i < n; i ++)
  {
	fprintf(fpa, "\t%lf\t%lf\n", x[i], camy[i]);
  }
fclose(fpa);

fpa = fopen("thickness_surface.dat", "w"); 	

for(i = 0; i < n; i ++)
  {
	fprintf(fpa, "\t%lf\t%lf\n", x[i], airy[i]);
  }
fclose(fpa);


fpa = fopen("upper_surface.dat", "w"); 	

for(i = 0; i < n; i ++)
  {
	fprintf(fpa, "\t%lf\t%lf\n", x[i], yu[i]);
  }
fclose(fpa);


fpa = fopen("lower_surface.dat", "w"); 	

for(i = 0; i < n; i ++)
  {
	fprintf(fpa, "\t%lf\t%lf\n", x[i], yl[i]);
  }

fclose(fpa);

//writing into input.dat

fpa = fopen("input.dat", "w"); 	

for(i = 0; i < n; i ++)
  {
	fprintf(fpa, "%lf\t%lf\n", x[i], yl[i]);
  }

for(i = n-1; i >=0; i --)
  {
	fprintf(fpa, "%lf\t%lf\n", x[i], yu[i]);
  }

fclose(fpa);











}




//*********************************************************************************************************************************



int main()
{

double aoa = 0.; 			// Angle of attack, in degrees
int naca = 12;
int nj = 140; 				// No of panel end points 	
int ni = 139; 				// No of panels				
double x_c[nj];				// X co-ordinates of panel end-points in GCS (Global co-ordinate system)
double y_c[nj];				// Y co-ordinates of panel end-points in GCS
int i;	

FILE *fp;
int nc = 5;		// no of control points for camberline curve
int na = 6;		// no of control points for airfoil curve

double cx[nc];		// coordinates of control pointsn
double cy[nc];
double ax[na];
double ay[na];
double cft = 0;

	FILE *fout; 
			// Y co-ordinates of panel end-points in GCS
char c[100];
char **end;
double badret = -100;

float cl,cd,cdpx,cm,trans_top,trans_bot, temp;
double cldt=0;
double clf=0;
double cldp=0;
double cdp=0;
double cld=0;
double txu, txl;

//writing out arbit data in case the goementry is not solvable
fout = fopen("data_output.dat","w");
//fprintf(fout, "%lf\n%lf\n%lf\n%lf\n%lf\n%lf\n%lf\n%lf\n",badret, badret,badret,badret,-1*badret,-1*badret,badret,badret);
fprintf(fout, "%lf\n%lf\n%lf\n",badret, badret,badret);
fclose(fout);
//reading in the points needed to generate the bezier curve
FILE *fin = fopen("data_input.dat","r");
double Re,q,nu;

fscanf(fin, "%lf", &ax[2]);
fscanf(fin, "%lf", &ax[3]);
fscanf(fin, "%lf", &ax[4]);
fscanf(fin, "%lf", &ay[1]);
fscanf(fin, "%lf", &ay[2]);
fscanf(fin, "%lf", &ay[3]);
fscanf(fin, "%lf", &ay[4]);
fscanf(fin, "%lf", &cx[1]);
fscanf(fin, "%lf", &cx[2]);
fscanf(fin, "%lf", &cx[3]);
fscanf(fin, "%lf", &cy[1]);
fscanf(fin, "%lf", &cy[2]);
fscanf(fin, "%lf", &cy[3]);

 
/*
fscanf(fin, "%lf", &ay[1]);
fscanf(fin, "%lf", &ay[2]);
fscanf(fin, "%lf", &ay[3]);
fscanf(fin, "%lf", &ay[4]);
fscanf(fin, "%lf", &cy[1]);
fscanf(fin, "%lf", &cy[2]);
fscanf(fin, "%lf", &cy[3]);
*/

fscanf(fin, "%lf", &aoa);
fscanf(fin, "%lf", &Re);
fscanf(fin, "%lf", &q);
fscanf(fin, "%lf", &nu);
fclose(fin);



/////////////////////////////////////////////////////////////////////////////////////////////////////


// endpoints of the points used to generate bezier curve
cx[0] = 0;
cy[0] = 0;
ax[0] = 0;
ay[0] = 0;

cx[nc-1] = 1;
cy[nc-1] = 0;
ax[na-1] = 1;
ay[na-1] = 0.0012;

ax[1] = 0;

		

// input points 


//extra constrains 
/*
ax[2] = 0.2;
ax[3] = 0.5;
ax[4] = 0.75;

cx[1] = 0.3;
cx[2] = 0.5;
cx[3] = 0.7;
*/
bezier(nc, na, cx, cy, ax, ay);




//run xfoil to generate the necessary parameters

if (gloflag == 0)
{

	system("./self.sh");


FILE *fp;

fp = fopen( "input.dat", "r" ); 
for (i = 0; i < nj; i++)
{				
	fscanf(fp, "%lf %lf", &x_c[i], &y_c[i]); 
	//printf("%d %lf %lf \n", i, x_c[i], y_c[i]); 
	
}

//read the Cd, Cl, and Cf from the file written by xfoil
fin = fopen("output.pol","r");

// buffering the excess data
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);


fscanf(fin, "%s", c);
printf("%s\n",c);
cl = atof(c);
fscanf(fin, "%s", c);
printf("%s\n",c);
cd = atof(c);
fscanf(fin, "%s", c);
printf("%s\n",c);
cdpx = atof(c);
fscanf(fin, "%s", c);
printf("%s\n",c);
cm = atof(c);
fscanf(fin, "%s", c);
printf("%s\n",c);
trans_top = atof(c);
fscanf(fin, "%s", c);
printf("%s\n",c);
trans_bot = atof(c);

printf("\n\n%lf\n%lf\n%lf\n%lf\n%lf\n%lf",cl,cd,cdpx,cm,trans_top,trans_bot);
cldt = cl/cd;
printf("\n%lf",cldt);

if( cd ==0)
{
cldt= -100000;
gloflag =1;
}
fclose(fin);




double pi = (22./7.);
double M = 0.14; 						// Mach Number
double th[ni]; 							// Panel orientation angle, AoA  in radians
double cth[ni], sth[ni]; 				// Cosine and sine of panel orientation angles

double Cp[nj], Cp_ep[nj];				//Coefficient of pressure at mid-points and end-points
double ue_ep[nj], ds[ni]; 			// Tangential component of total (edge+free-stream) velocity at end-points and length of the panels respectively
double Cp_max; 

FILE *fpa, *fpb; 						// File pointer DECLARATION
int j, imax, n_u, n_l; 				// Counters

double x_cd[2][ni], y_cd[2][ni], ued[2][ni], cpd[2][ni];


// read the Cp data written by xfoil
fin = fopen("cpdata.dat","r");

fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);

for( i =0;i<nj;i++)
{
	fscanf(fin, "%s", c);
	fscanf(fin, "%s", c);
//	printf("%s\n",c);
	temp = atof(c);
	Cp_ep[i] = temp;
}

fclose(fin);


// read the velocity data written by xfoil
fin = fopen("uedata.dat","r");

fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);
fscanf(fin, "%s", c);

for( i =0;i<nj;i++)
{
	fscanf(fin, "%s", c);
	fscanf(fin, "%s", c);
	fscanf(fin, "%s", c);
	fscanf(fin, "%s", c);
//	printf("%s\n",c);
	temp = atof(c);
	ue_ep[i] = temp;
	fscanf(fin, "%s", c);
	fscanf(fin, "%s", c);
	fscanf(fin, "%s", c);
	fscanf(fin, "%s", c);

}

fclose(fin);




//writing the x coordinates, y coordinates, velocity data and cp values for the uppwe surface and lower surface


fpa = fopen("x_y_ue_cp_nu_upper_surface.dat", "w"); 	
fpb = fopen("x_y_ue_cp_nl_lower_surface.dat", "w");

for (i = 0; i < ni; i++)
{


/*Panel angle*/
th[i] = atan2((y_c[i+1] - y_c[i]), (x_c[i+1] - x_c[i]));  	//Panel Orientation Angle using tan inverse of (y/x)

/*To find cos and sin of the angle*/
cth[i] = cos(th[i]);
sth[i] = sin(th[i]);

//printf("%lf  %lf  %lf\n", x_c[i], sth[i], cth[i]);
}

//X and Y co-ordinates in panel co-ordinate system

for(i = 0; i < ni; i++)
{
ds[i] = sqrt(pow( (x_c[i+1]-x_c[i]),2) + pow( (y_c[i+1]-y_c[i]),2) );  	// Length of each panel


}


// To determine the index of the stagnation point ( Cp_ep[i] has maximum value at the stagnation point )

Cp_max = 0;

for(i = 3; i < ni-3; i ++)
{

if(Cp_ep[i] > Cp_max)

{

Cp_max = Cp_ep[i];

imax = i;

}

}
//printf("\n\n Cp_max = %lf at the point i = %d (Stagnation Point)\n\n", Cp_max, imax);

// Write a file for (x,y), ue and n for upper surface

for(i = (imax); i < nj; i ++)
{
for(j = (i-imax); j < (ni - imax +1); j ++)
{
x_cd[0][j] = x_c[i]; // The 'd' stands for a different nomenclature for x, y, Ue_t and Cp_ep

y_cd[0][j] = y_c[i];

ued[0][j] = fabs(ue_ep[i]);
cpd[0][j] = Cp_ep[i];

n_u = ni - imax +1;

}
}

fprintf(fpa, "%d\n", n_u);

//fprintf(fpa, "i         x_c_u        y_c_u         ue_u \n");

for(i = 0; i < n_u; i ++)

{

fprintf(fpa, "\t%lf\t%lf\t%lf\t%lf\n", x_cd[0][i], y_cd[0][i], ued[0][i], cpd[0][i]);

}

fclose(fpa);

// Write a file for (x,y), ue and n for lower surface


for(i = 0; i < (imax+1); i ++)
{
for(j = (imax-i) ; j >= 0; j--) 
{
x_cd[1][j] = x_c[i];

y_cd[1][j] = y_c[i];

ued[1][j] = fabs(ue_ep[i]);
cpd[1][j] = Cp_ep[i];

n_l = imax+1;

}
}

fprintf(fpb, "%d\n", n_l);

//fprintf(fpb, "i	     x_c_l	   y_c_l	  ue_l  \n");

for(i = 0; i < (n_l) ; i ++)
{
fprintf(fpb, "\t%lf\t%lf\t%lf\t%lf\n", x_cd[1][i], y_cd[1][i], ued[1][i], cpd[1][i]);
}

fclose(fpb);


//FILE *fpa;
//FILE *fpt;
FILE *fpru;
FILE *fprl;


/* INPUTS*/
int mb = 200;  			// MAX NO. OF POINTS TO BE CONSIDERED IN THE LOOP WHICH CALCULATES ALL THE BOUNDARY LAYER PARAMETERS
int np_us, np_ls; 		// NUMBER OF POINTS ON THE UPPER AND LOWER SURFACES
int i_vel_us = 10;		// LOCATION ON THE UPPER SURFACE AT WHICH VELOCITY PROFILE IS REQUIRED 
int i_vel_ls = 10; 		// LOCATION ON THE LOWER SURFACE AT WHICH VELOCITY PROFILE IS REQUIRED 
int nbl = 200;  		// NUMBER OF POINTS IN THE BOUNDARY LAYER
double nu = 1.583E-05; 	// KINEMATIC VISCOSITY AT 27deg C in M^2/SEC (Ref: engineering toolbox)
double Re = 3E06;
double q = 0.1; 		// Freestream turbulence intensity 


double s[mb];
double ue[mb];
double theta[mb];  	// MOMENTUM THICKNESS 
double delta[mb]; 	// BOUNDARY LAYER THICKNESS 
double pgp[mb];  	// PRESSURE GRADIENT PARAMETER  delta2/nu*dueds 
double H[mb];  		// SHAPE FACTOR 
double dueds[mb]; 	// Edge velocity gradient 
double cf[mb]; 		// Skin friction co-efficient
double cp[mb];
double lambda[mb]; 		// Pressure gradient parameter
//double pi = 3.141593;
double rad = pi/180;
double K = 0.45/Re;
double L;
double coeff = sqrt(3.0/5.0);
double f1, f2, f3;
double xm, dx;
double dth2ue6;
double rex[mb], ret[mb], retmax, rex_turb, rex_ft; 
double fs, gs;
double sti[2], ste, ltr;
double lamda_trans[2], arg, interm_fact[mb]; 
double cf_total[mb], cf_total_trans_zone[mb], cf_total_ft;
double factor;
double x_cL[mb], x_cU[mb], y_cL[mb], y_cU[mb];
double xc[2][mb], yc[2][mb], ue_d[2][mb], cp_d[2][mb];

int n;
int trans;
int itrans[2], iturb[2], iturbU, iturbL;
i=0;j=0;

fpru = fopen("x_y_ue_cp_nu_upper_surface.dat", "r");
fprl = fopen("x_y_ue_cp_nl_lower_surface.dat", "r");

//fpa = fopen("bl_ue_cp_output.dat", "w");
//fpt = fopen("turbulent_skin_friction.dat", "w");

//printf("\nFREE STREAM REYNOLDS NUMBER Re = %f\n\n", Re);
//printf("(i traverses from Stagnation Point to the Trailing Edge)\n");
//printf("(cpd = Pressure Coefficient at the ith point)\n");
//printf("(ued = Tangential Component of Total(Edge + Free Stream) Velocity at ith end-point)\n\n\n");
// Start of data reading 

// Upper surface

if (fpru == NULL)
return 0;

fscanf(fpru, "%d\n", &np_us);  ///SCANNING NUMBER OF POINTS ON THE UPPER SURFACES
//printf("No. of points in Upper surface (including stagnation point) = %d \n\n", np_us);

//printf("i   x/c    y/c  	ued	  cpd\n\n");  
for (i = 0; i < np_us; i ++)   
{				

	fscanf(fpru,"%lf\t%lf\t%lf\t%lf", &xc[0][i], &yc[0][i], &ue_d[0][i], &cp_d[0][i]);  ///Scanning x,y and ued for upper surface
	//printf("%d %lf %lf %lf %lf\n", i, xc[0][i], yc[0][i], ue_d[0][i], cp_d[0][i]); 

}

//printf("\n\n");

// Lower surface

if (fprl == NULL)
return 0;

fscanf(fprl, "%d\n", &np_ls);
//printf("No. of points in Lower surface (including stagnation point) = %d \n\n", np_ls);

//printf("i	x/c    y/c  	ued	  cpd\n\n");
for (i = 0; i < np_ls; i ++)  
{				

	fscanf(fprl,"%lf\t%lf\t%lf\t%lf", &xc[1][i], &yc[1][i], &ue_d[1][i], &cp_d[1][i]);
	//printf("%d %lf %lf %lf %lf\n", i, xc[1][i], yc[1][i], ue_d[1][i], cp_d[1][i]); 

}
// Data reading finished


//printf("\nBOUNDARY LAYER PARAMETERS\n\n");
//printf(" delta_c = Boundary Layer Thickness");
//printf("\n theta_c = Momentum Thickness");
//printf("\n H_c = Shape Factor");
//printf("\n cf_c = Skin Friction Coefficient");
//printf("\n Retheta = Reynold's No. based on Momentum thickness");
//printf("\n lambda = Thwaites Pressure Gradient Parameter\n");

for(j=0; j <= 1; j++) 	// J loop opened (it gets closed just before main closes, j can be either 0 or 1)
{

if(j == 0) mb = np_us;	//Upper surface
else mb = np_ls; 		//	Lower surface

trans = 0;

for(i=0; i < mb; i++)   // I1 loop opened  CREATING COMPUTATIONAL POINTS 
{
	if(i==0) s[0] = 0.;			
	else s[i] = s[i-1] + sqrt(pow((xc[j][i] - xc[j][i-1]),2) + pow((yc[j][i] - yc[j][i-1]),2)); //Distance of a computational point from the stagnation point
																									//Distance is measured here via all the included panels 
   	ue[i] = ued[j][i];  // BOUNDARY LAYER EDGE VELOCITY 

} // I1 loop closing


//if(j==0) printf("\n\n BOUNDARY LAYER PARAMETERS FOR UPPER SURFACE \n");
//else printf("\n\n BOUNDARY LAYER PARAMETERS FOR LOWER SURFACE \n");

//printf("\ni  delta_c  theta_c  H_c  cf_c  x_c  Retheta  lambda\n\n");

dueds[0] = (ue[1] - ue[0])/s[1]; 							// Edge velocity gradient at First point

dueds[mb-1] = (ue[mb-1] - ue[mb-2])/(s[mb-1] - s[mb-2]); 	// Edge velocity gradient at Last point 
															///How to calculate the gradient of the last point (mb-1)
theta[0] = sqrt(0.075/(Re*dueds[0]));  						// Computation of momentum thickness at Stagnation point

lambda[0] = pow(theta[0],2)*dueds[0]*Re; 					// Thwaites pressure gradient parameter, based on momentum thickness, at stagnation point
															
H[0] = fH(lambda[0]);                     					// Computation of shape factor


/* COMPUTATION OF BOUNDARY LAYER THICKNESS AND PROFILE */

pgp[0] = fPGP(H[0]);

delta[0] = theta[0]/fDELTA(pgp[0]); // Eqn 12.24, Bou. Lay. Th, Schlichting (1960)

L = fL(lambda[0]); 					// Skin friction parameter

cf[0] = 2*L/ue[0]/(Re*theta[0]); 	// Coefficient of skin friction

i=0;///*
//printf("%d %f %f %f %f \n", i, delta[i], theta[i], H[i], cf[i]);    ///in the first iteration it will print 0, delta[0], theta[0], H[0], cf[0]
																	///these are the values at stagnation point(i=0) so why are these different for up and low surface

for(i=1; i<(mb-1); i++) dueds[i] = (ue[i+1]-ue[i-1])/(s[i+1]-s[i-1]); // COMPUTATION OF EDGE VELOCITY GRADIENT d(ue)/ds


for(i=1; i < mb; i++) // I2 loop opening
{

	//Numerical integration by Gaussian 5th order quadrature method

	xm = (s[i]+s[i-1])/2;
	dx = (s[i]-s[i-1]);
	f1 = fU(xm-coeff*dx/2, s[i-1], ue[i-1], dueds[i-1]); 
	f1 = pow(f1,5);                             	//coeff = sqrt(3.0/5.0)
	f2 = fU(xm, s[i-1], ue[i-1], dueds[i-1]); 
	f2 = pow(f2,5);
	f3 = fU(xm+coeff*dx/2, s[i-1], ue[i-1], dueds[i-1]); 
	f3 = pow(f3,5);
	
	dth2ue6 = K*dx/18*(5*f1+8*f2+5*f3);     		// K = 0.45/Re

	theta[i] = sqrt((pow(theta[i-1],2)*pow(ue[i-1],6) + dth2ue6)/pow(ue[i],6)); // Computation of momentum thickness-Sankar(GATECH) ppt. Here theta = Momentum thickness per unit characteristic Length.

	lambda[i] = pow(theta[i],2)*dueds[i]*Re; 		// Thwaites PGP along the contour, this will give a numerical value of lambda which should not be -0.14 or -0.139
													// Schlisting defines a similar quantity and refers it as Second Shape Factor(Eqn 12.27, Pg 246)				
	if(lambda[i] == -0.14) lambda[i] = -0.139; 		// To prevent 'undefined/indeterminate situations during calculation of H[i]

	if(lambda[i] == -0.107) lambda[i] = -0.106;


	H[i] = fH(lambda[i]); /* COMPUTATION OF SHAPE FACTOR AT A POINT, Shape Factor(H) = (disp. thickness)/(Mom. thickness) */

	/* COMPUTATION OF BOUNDARY LAYER THICKNESS AND PROFILE */

	pgp[i] = fPGP(H[i]);

	delta[i] = theta[i]/fDELTA(pgp[i]); // Eqn 12.24, Boundary Layer Theory, Schlichting (1960)

	if(j==0 && i==i_vel_us) fprofileU(nbl, pgp[i_vel_us], delta[i_vel_us]); // PRINTING B.L. VELOCITY PROFILE OUTPUT AT THE GIVEN LOCATION ON THE UPPER SURFACE
	if(j==1 && i==i_vel_ls) fprofileL(nbl, pgp[i_vel_ls], delta[i_vel_ls]);	// PRINTING B.L. VELOCITY PROFILE OUTPUT AT THE GIVEN LOCATION ON THE LOWER SURFACE

	rex[i] = Re*s[i]*ue[i];	// Reynolds number at any given point

	L = fL(lambda[i]); 		// Skin friction parameter at a point

	while(trans != 2) 
	{

	cf[i] = 2*L/ue[i]/(Re*theta[i]);  /* COMPUTATION OF SKIN FRICTION COEFFICIENT AT A POINT */

	break;

	}
	
	//till here data is required for the matlab code



	while(trans == 0)    /* APPLICATION OF TRANSITION CRITERIA (trans=0 is FLOW BEFORE THE POINT OF TRANSITION:LAMINAR FLOW) */
	{

	ret[i] = Re*theta[i]*ue[i];	// Momentum thickness based Re  

	retmax = fTRONSET(ret[i], q, lambda[i]);  // Maximum_ReTheta, for identification of location of onset of transition ; q = Freestream turbulence intensity

	while(ret[i]>retmax)
	{
	trans = 1;     
	itrans[j] = i;
	sti[j] = s[i];
	lamda_trans[j] = fTE(rex[i], ue[i], theta[i], Re, lambda[i]); // Estimation of the extent of transition zone

	//if(j==0) printf("\n(TRANSITION LOCATION FOR UPPER SURFACE)\n");
	//else printf("\n(TRANSITION LOCATION FOR LOWER SURFACE)\n");
	
	//printf("\n\titrans = %d\n", itrans[j]);
	//printf("\tTransition Point x_c = %lf\n", xc[j][i]);
	//printf("\tDistance of the Transition Point from the Stagnation Point (measured along Panels) = %lf\n", sti[j]);
	//printf("\tEstimated Length of the Transition Zone (measured along the panels) = %lf\n", lamda_trans[j]);
	//printf("\tReynolds No. at this point, rex = %lf\n", rex[i]);
	//printf("\tMomentum thickness based Reynolds No., ret = %lf\n\n", ret[i]);
	//printf("\tTransition zone ends (i = %d) at x_c = %lf\n\n", iturb[j], x_c[p][j]);	

	break;
	}
	break;
	}

	//printf("%d %lf %lf %lf %lf %lf %lf %lf \n", i, delta[i], theta[i], H[i], cf[i], xc[j][i], ret[i], lambda[i]);
	
	while(trans == 1)   /* FLOW BEYOND THE POINT OF TRANSITION - Transition zone modeling*/
	{

	/* CALCULATION OF INTERMITTENCY FACTOR IN TRANSITION ZONE */
	/**********************************************************/

	arg = 0.412*pow(((s[i] - sti[j])/lamda_trans[j]),2.);

	interm_fact[i] = 1.;  ///if arg>20 then exp(-arg) is very small and interm_fact is~1

	while(arg < 20.)
	{
	interm_fact[i] = 1. - exp(-arg);
	break;
	}


	while(interm_fact[i] > 0.99)
	{
	trans = 2; // 	Indicating end of transition zone and beginning of fully turbulent flow
	interm_fact[i] = 1.;
	break;
	}
	break;
	}

	/* COMPUTATION OF TOTAL SKIN FRICTION COEFFICIENT */
	/**********************************************************/
	rex_turb = Re*(s[i]-sti[j])*ue[i];

	cf_total[i] = 0.295*pow((log10(rex_turb)), -2.45);  // Boeing correlation (by Schulz & Grunow, modified by Kulfan at Boeing) for coeff of Skin friction in fully turbulent flow

	cf_total_trans_zone[i] = (1. - interm_fact[i])*cf[i] + interm_fact[i]*cf_total[i]; 		//this Cf care of all three Zones
		
	//printf("%d %lf %lf %lf %lf %lf %lf %lf \n", i, delta[i], theta[i], H[i], cf_total_trans_zone[i], xc[j][i], ret[i], lambda[i]);
	
	if(j==1 && i==mb-1){
		int k;
		for(k=0; k < mb; k++) x_cL[k] = xc[1][k]; 
		fparameterL(mb, delta, theta, H, cf_total_trans_zone, x_cL, ret, lambda);
	}

	if(j==0 && i==mb-1){
		int m;
		for(m=0; m < mb; m++) x_cU[m] = xc[0][m]; 
		fparameterU(mb, delta, theta, H, cf_total_trans_zone, x_cU, ret, lambda);
	}
	
	if(i==mb-1 && j==0){
		int m;
		for(m=0; m < mb; m++) x_cU[m] = xc[0][m];
		fturbSkinU(mb, s, rex, x_cU, sti, interm_fact, cf_total_trans_zone, lamda_trans, itrans);
	} 
	
	if(i==mb-1 && j==1){
		int m;
		for(m=0; m < mb; m++) x_cL[m] = xc[1][m];
		fturbSkinL(mb, s, rex, x_cL, sti, interm_fact, cf_total_trans_zone, lamda_trans, itrans);
	}
	int h;
	if(i==mb-1 && j==0) for(h=0; h < mb; h++) y_cU[h] = yc[0][h];
	if(i==mb-1 && j==1){
		int m;
		for(m=0; m < mb; m++) y_cL[m] = yc[1][m];
		fZones(x_cU, x_cL, y_cU, y_cL, np_us, np_ls, itrans, lamda_trans, sti);
	}

if(interm_fact[i]>.99) interm_fact[i+1]=1.; //As intermittency factor in the turbulence zone is 1

} // I2 loop closing 
//fclose(fpa);
//fclose(fpt);


if( j==1)
{
for ( i =0; i<n_l; i++)
	{
//			cft = cft + cf_total_trans_zone[i]*sth[n_l-i-1]*ds[n_l-i-1]*-1;
			cft = cft + cf_total_trans_zone[i]*ds[n_l-i-1];
//			printf("%d  %lf\t",n_l-i-1, cf_total_trans_zone[i]);
//			printf("%lf\n", cf_total_trans_zone[i]*sth[i]*ds[i]*-1);
	}
}
else
{
for ( i =0; i<n_u; i++)
	{
//			cft = cft + cf_total_trans_zone[i]*sth[i+n_l-1]*ds[i+n_l-1]*-1;
			cft = cft + cf_total_trans_zone[i]*ds[i+n_l-1];
//			printf("%d  %lf\t",i+n_l-1, cf_total_trans_zone[i]);
//			printf("%lf\n", cf_total_trans_zone[i]*sth[i+n_l]*ds[i+n_l]);
	}
}





//printf("\n\n");
} // J loop closing
//cft =0;



clf = cl/cft;
cdp = cd-cft;
cldp = cl/cdp;
cld = cl/cd;
txu = x_cU[itrans[0]];
txl = x_cL[itrans[1]];

printf("\n%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t", cl,cld,cldp,clf,cdp,cft,txu,txl); 


}// end for the if condition 


if(isnan(cft)!=0)
	gloflag =1;



if( gloflag ==0)
{
fout = fopen("data_output.dat","w");
//fprintf(fout, "%lf\n",cldt);
//fprintf(fout, "%lf\n%lf\n%lf\n",cldt/300, txu, txl);
//fprintf(fout, "%lf\n%lf\n%lf\n%lf\n%lf\n%lf\n%lf\n%lf\n",cl,cld,cldp,clf,cdp,cft,txu,txl);
fprintf(fout, "%lf\n%lf\n%lf\n",cld,txu,txl);
fclose(fout);
}
else
{
fout = fopen("data_output.dat","w");
//fprintf(fout, "%lf\n",badret);
//fprintf(fout, "%lf\n%lf\n%lf\n",badret, badret, badret);
fprintf(fout, "%lf\n%lf\n%lf\n",badret, badret,badret);
fclose(fout);
}

printf ("\n\n\n%d",gloflag);




} // main() function closing




//*****************************************************************************************************
/*                FUNCTIONS DEFINED                         */
//*****************************************************************************************************



//Function used for computation of shape factor H[]=fH(lambda),lambda=Pr. Gr. Parameter

double fH(double x)
{
double d;

while(x>=0.)			//Favorable pr. gr.
{
d = 2.61 - 3.75*x + 5.24*pow(x,2);// Moran (2003) - reference in Pr_Gr_Param_refs.pdf avlbl on Docz
break;
}

while(x<0.)				//Adverse pr. gr.
{
d = 2.088 + 0.0731/(x+0.14);
while(d > 7.0)
{
d = 2.472 + 0.0147/(0.107 + x);
break;
} 
while(d < 0.)
{
d = 2.472 + 0.0147/(0.107 + x);
break;
}   
  
break;
}    
return ( d ) ;
} // fH function closing



//*****************************************************************************************************



//Function for Skin friction parameter ///L=fL(lambda),lambda=Pr. Gr. Parameter

double fL(double y)
{
double z;

while(y>=0.)
{
z = 0.22 + 1.57*y - 1.8*pow(y,2); // Moran (2003) - reference in Pr_Gr_Param_refs.pdf avlbl on Docz
break;
}

while(y<0.)
{
z = 0.22 + 1.402*y +(0.018*y)/(y+0.107);
break;
}    
return ( z ) ;
} // fL function closing



//*****************************************************************************************************



// "im1" everywhere stands for (i-1), and is a short form of (i minus 1)

double fU(double a, double sim1, double ueim1, double duedsim1)
{
double b;

b = ueim1 + duedsim1*(a-sim1);

return (b);

} // fU function closing



//*****************************************************************************************************



// Function for Pohlhausen's pressure gradient parameter (PGP), used in determining the BL velocity profile, also for calculating B.L. Thickness
// Schlisting refers it as First Shape Factor (Eqn 12.21, Pg 244)
// pgp[i]=fPGP(H[i]). Not defined for 0.056 < H[i] < 2.256 which is the Non-laminar region

double fPGP(double H)
{
double xm;
double A, B, C;

double bsm4ac;

A = H/9072.;
B = H/945. - 1./120.;
C = 3./10. -37./315.*H;  ///this is (37./315.)*H 
 
bsm4ac = B*B - 4.*A*C;
xm = (-B - sqrt(bsm4ac))/(2.*A);

return (xm);
} // fPGP function closing

 

//*****************************************************************************************************



double fDELTA(double pgp)
{
double arg;
arg = 37./315. - 1./945.*pgp - 1./9072.*pgp*pgp;
return(arg);
} // fDELTA function closing



//*****************************************************************************************************



//Function To determine the extent of transition zone 

double fTE(double rex, double uei, double thetai, double Re, double lambda)
{
double ltr, N;

//N = 0.7E-03; 

N = 5E-03 + 0.25*lambda*lambda; // From Prog. in Aero. Sci paper by Narasimha - for low freestream turbulence intensity of 0.02% - Pg 50

ltr = sqrt(0.41*uei*pow(thetai,3.)*Re/N); // J. of Aircraft paper (Oct 1990) - Dey and Narasimha

return (ltr);

} // fTE function closing.



//******************************************************************************************************



// Function to determine the location of onset of transition
/*  Various correlations have been tried out. The Narasimha and Dey Correlation and Langtry correlation (given at the end of the list)
provide reasonable accurate prediction, by taking into account effects of pressure gradient and free-stream turbulent intensity.  */

double fTRONSET(double rex, double q, double lambda)
{

/* Narasimha and Dey Criteria */

//retmax = 0.9*(100. + 310./q)*(1. + 0.15*(exp(-q) + 2.)*(1. - exp(-60.*lambda)));


//Menter and Langtry : P Malan AIAA 2009 paper


//retmax = 803.73*pow((q+0.6067), -1.027) ; // Zero pressure gradient case

// Modification by Langtry (Eqn. 24) of the same paper: For Tu < 1.3 %
	
double retmax;
if(lambda>0) retmax = (1173.51 - 589.428 * q + 0.2196/q/q)*(1+ 0.275*exp(-2*q)*(1-exp(-35*lambda)));
else retmax = (1173.51 - 589.428 * q + 0.2196/q/q)*(1 + exp(-(pow((2*q/3),1.5)))*(12.986*lambda+123.66*lambda*lambda+405.689*lambda*lambda*lambda));

return(retmax);
}



//******************************************************************************************************



// PRINTING B.L. VELOCITY PROFILE OUTPUT AT THE GIVEN LOCATION ON THE UPPER SURFACE

double fprofileU(int nbl, double pgp, double delta)
{
//printf("\ni      y/delta    y/L     Velocity\n\n");
//fprintf(fpc, "\ni      y/delta    y/L     Velocity\n\n"); 

FILE *fpcu;
fpcu = fopen("bl_vel_profile_upper_surface.dat", "w");

double deta = 1./(nbl-1);
double eta = 0.;
double fs, gs, zeta, vel[nbl];
int i;
for(i = 0; i < nbl; i++) // I3A loop opening		//For obtaining the velocity profile: u/U = F(eta)+lamda*G(eta)
{
	fs = 1. - pow((1. - eta), 3)*(1. + eta);  	// Pg 244, Schlicthing
	gs = 1./6.*eta*pow((1. - eta), 3);
	vel[i] = fs + pgp*gs;   				//i_vel = LOCATION AT WHICH VELOCITY PROFILE IS REQUIRED
	zeta = eta*delta;
	
	
	///printf("%d %f %f %f\n", i, eta, zeta, vel[i]);
	fprintf(fpcu, "%d %f %f %f\n", i, eta, zeta, vel[i]);
			
	eta = eta + deta; 
} // I3 loop closing
fclose(fpcu);


}	



//******************************************************************************************************



// PRINTING B.L. VELOCITY PROFILE OUTPUT AT THE GIVEN LOCATION ON THE LOWER SURFACE

double fprofileL(int nbl, double pgp, double delta)
{
//printf("\ni      y/delta    y/L     Velocity\n\n");
//fprintf(fpc, "\ni      y/delta    y/L     Velocity\n\n"); 

FILE *fpcl;
fpcl = fopen("bl_vel_profile_lower_surface.dat", "w");

double deta = 1./(nbl-1);
double eta = 0.;
double fs, gs, zeta, vel[nbl];
int i;
for(i = 0; i < nbl; i++) // I3A loop opening		//For obtaining the velocity profile: u/U = F(eta)+lamda*G(eta)
{
	fs = 1. - pow((1. - eta), 3)*(1. + eta);  	// Pg 244, Schlicthing
	gs = 1./6.*eta*pow((1. - eta), 3);
	vel[i] = fs + pgp*gs;   				//i_vel = LOCATION AT WHICH VELOCITY PROFILE IS REQUIRED
	zeta = eta*delta;
	
	
	///printf("%d %f %f %f\n", i, eta, zeta, vel[i]);
	fprintf(fpcl, "%d %f %f %f\n", i, eta, zeta, vel[i]);
			
	eta = eta + deta; 
} // I3 loop closing
fclose(fpcl);


}



//*****************************************************************************************************



// FUNCTION TO CALCULATE THE INVERSE OF A MATRIX
#define abs(a) ((a)<0 ?(-(a)):(a)) 

int inverse(int n, double A[], double AA[])
{
  int *ROW, *COL;
  double *TEMP;
  int HOLD , I_pivot , J_pivot;
  double pivot, abs_pivot;
  int i, j, k;

  ROW = (int *)calloc(n, sizeof(int)); ///allocate storage. Returns a pointer to enough free space for an array of n objects of the specified size
  COL = (int *)calloc(n, sizeof(int));
  TEMP = (double *)calloc(n, sizeof(double));
  memcpy(AA, A, n*n*sizeof(double));   ///String operation, copies given no. of chars from A to AA

  /*              set up row and column interchange vectors */
  for (k=0; k<n; k++)
  {
    ROW[k] = k ;
    COL[k] = k ;
  }

  /*               begin main reduction loop */
  for (k=0; k<n; k++)
  {
    /*             find largest element for pivot */
    pivot = AA[ROW[k]*n+COL[k]] ;
    I_pivot = k;
    J_pivot = k;
    for(i=k; i<n; i++)
    {
      for(j=k; j<n; j++)
      {
        abs_pivot = abs(pivot) ;
        if( abs(AA[ROW[i]*n+COL[j]]) > abs_pivot )
        {
          I_pivot = i ;
          J_pivot = j ;
          pivot = AA[ROW[i]*n+COL[j]] ;
        }
      }
    }
    if(abs(pivot) < 1.0E-65)
    {
      free(ROW);
      free(COL);
      free(TEMP);
      printf("MATRIX is SINGULAR !!! \n");
      return -1;
    }

    HOLD = ROW[k];
    ROW[k]= ROW[I_pivot];
    ROW[I_pivot] = HOLD ;
    HOLD = COL[k];
    COL[k]= COL[J_pivot];
    COL[J_pivot] = HOLD ;

    /*               reduce about pivot */
    AA[ROW[k]*n+COL[k]] = 1.0 / pivot ;
    for (j=0; j<n; j++)
    {
      if( j != k )
      {
        AA[ROW[k]*n+COL[j]] = AA[ROW[k]*n+COL[j]] * AA[ROW[k]*n+COL[k]];
      }
    }

    /*               inner reduction loop */
    for (i=0; i<n; i++)
    {
      if( k != i )
      {
        for (j=0; j<n; j++)
        {
          if( k != j )
          {
            AA[ROW[i]*n+COL[j]] = AA[ROW[i]*n+COL[j]] - AA[ROW[i]*n+COL[k]] *
                                  AA[ROW[k]*n+COL[j]] ;
          }
        }
        AA[ROW[i]*n+COL [k]] = - AA[ROW[i]*n+COL[k]] * AA[ROW[k]*n+COL[k]] ;
      }
    }
  }
  /*      end of main reduction loop */

  /*      unscramble rows */
  for (j=0; j<n; j++) {
    for (i=0; i<n; i++) {
      TEMP[COL[i]] = AA[ROW[i]*n+j];
    }
    for (i=0; i<n; i++) {
      AA[i*n+j] = TEMP[i] ;
    }
  }

  /*      unscramble columns */
  for (i=0; i<n; i++)
  {
    for (j=0; j<n; j++)
    {
      TEMP[ROW[j]] = AA[i*n+COL[j]] ;
    }
    for (j=0; j<n; j++)
    {
      AA[i*n+j] = TEMP[j] ;
    }
  }

  free(ROW);
  free(COL);
  free(TEMP);
} /* end inverse */



//*****************************************************************************************************



// MATRIX MULTIPLICATION FUNCTION  --   Lifted from 3D_UNS code - common/vector_algebra.c

int mat_multi(int N, double *A, double *B, double *C)
{
  int i, j;

  for(i = 0; i < N; i++)
  {
    C[i] = 0;
    for(j = 0; j < N; j++)
      C[i] += A[i*N+j]*B[j];
  }
}// end of matrix multiplication



//*****************************************************************************************************
/*                END OF FUNCTIONS DEFINITION                         */
//*****************************************************************************************************
